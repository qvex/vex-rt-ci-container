FROM rustlang/rust:nightly-slim

ENV DEBIAN_FRONTEND=noninteractive
ENV VERSION=nightly-2024-01-01

RUN apt-get update -yqq
RUN apt-get install -yqq libclang-dev llvm-dev gcc-arm-none-eabi

RUN rustup install $VERSION
RUN rustup component add --toolchain=$VERSION rust-src rustfmt llvm-tools-preview clippy
